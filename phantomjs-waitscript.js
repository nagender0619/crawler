"use strict";

function waitFor(testFx, onReady, timeOutMillis) {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000*10, //< Default Max Timout is 3s
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
                // If not time-out yet and condition not yet fulfilled
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
            } else {
                if(!condition) {
                    // If condition still not fulfilled (timeout but condition is 'false')
                    console.log("'waitFor()' timeout");
                    phantom.exit(1);
                } else {
                    // Condition fulfilled (timeout and/or condition is 'true')
                    console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                    clearInterval(interval); //< Stop this interval
                }
            }
        }, 250); //< repeat check every 250ms
};



var page = require('webpage').create(),
    system = require('system'),
    t, address;

page.settings.userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.$";
phantom.cookiesEnabled = true;
phantom.javascriptEnabled = true;

phantom.clearCookies();

page.open(system.args[1]);
page.injectJs('jquery.min.js');
/* page.deleteCookie('KP_UID'); */
page.onLoadFinished = function(status) {
  
waitFor(function() {
  // Check in the page if a specific element is now visible
  return page.evaluate(function() {
    return typeof REA != 'undefined' && REA.propertyId == '1736699';
  });
}, function() {
  for (var i = 0; i < page.cookies.length; i++) {
    console.log(page.cookies[i].name + "=" + page.cookies[i].value);
  }
  page.render("google.png");
  console.log('StartObject' + page.evaluate(function () {
                
      var features = [];
      
      $('.property-info .property-info__attributes .rui-property-feature').each(function(){
        features.push({
          name: $(this).find('.rui-visuallyhidden').text(),
          count: $(this).find('.config-num').text()
        })
      });
      
      return JSON.stringify({
        'forSale': !$('.property-status-batch').hasClass('property-status-off-market'),
        'features': features,
        'estimatedCost': REA.avmData.range,
        'confidenceLevel': REA.avmData.confidence,
        'fullSuburb': REA.fullSuburb,
        'longStreetAddress': REA.longStreetAddress,
        'longStreetAddressWithSuburb': REA.longStreetAddressWithSuburb,
      });
  })+'EndObject');
  phantom.exit();
});
  
/*
var url = page.url;
for (var i = 0; i < page.cookies.length; i++) {
        console.log(page.cookies[i].name + "=" + page.cookies[i].value);
    }
console.log("Status: " + status);
console.log("Loaded: " + url);
page.render("google.png");*/
/* phantom.exit(); */
};