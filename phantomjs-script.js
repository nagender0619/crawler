"use strict";

var page = require('webpage').create(),
    system = require('system'),
    t, address;

page.settings.userAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36";
phantom.cookiesEnabled = true;
phantom.javascriptEnabled = true;

var c= phantom.addCookie({
  'name'     : 'KP_UID',   /* required property */
  'value'    : '98162b0ec2efb4ea4995c075e96ca266',  /* required property */
  'domain'   : 'www.realestate.com.au',        /* required property */
  'path'     : '/',
  'httponly' : true,
  'secure'   : false,
  'expires'  : (new Date()).getTime() + (1000 * 60 * 60)   /* <-- expires in 1 hour */
});

if (system.args.length === 1) {
    console.log('Usage: loadspeed.js <some URL>');
    phantom.exit(1);
} else {
    t = Date.now();
    address = system.args[1];
    page.open(address, function (status) {

      page.includeJs('https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js', function () {
        if (status !== 'success') {
            console.log('FAIL to load the address');
        } else {
            t = Date.now() - t;
            

            console.log('StartObject' + page.evaluate(function () {
                
                var features = [];
                
                $('.property-info .property-info__attributes .rui-property-feature').each(function(){
                  features.push({
                    name: $(this).find('.rui-visuallyhidden').text(),
                    count: $(this).find('.config-num').text()
                  })
                });
                
                return JSON.stringify({
                  'forSale': !$('.property-status-batch').hasClass('property-status-off-market'),
                  'features': features,
                  'estimatedCost': REA.avmData.range,
                  'confidenceLevel': REA.avmData.confidence,
                  'fullSuburb': REA.fullSuburb,
                  'longStreetAddress': REA.longStreetAddress,
                  'longStreetAddressWithSuburb': REA.longStreetAddressWithSuburb,
                });
            })+'EndObject');
            console.log('Loading time ' + t + ' msec');
        }
        phantom.exit();
      });
    });
}
