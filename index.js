var express = require('express')
var app = express()

app.set('port', (process.env.PORT || 5000))
app.use(express.static(__dirname + '/public'))

var path = require('path')
var childProcess = require('child_process')
var phantomjs = require('phantomjs-prebuilt')
var binPath = phantomjs.path
 

app.get('/', function(request, response) {
  
  var address = request.query.place;
  console.log('Received Request:'+address)
  address = address.replace(', Australia', '');
  address = address.replace(',Australia', '');
  address = address.replace('Australia', '');
  address = address.replace(/,/g, '');
  
  address = address.replace(/[&\/\\#,+()$~%.'":*?<>{}\ ]/g, '-');
  
  address = address.toLowerCase();
  
  var childArgs = [
    path.join(__dirname, 'phantomjs-waitscript.js'),
    'https://www.realestate.com.au/property/'+address+'?source=property-search-hp'
  ];
  
  childProcess.execFile(binPath, childArgs, function(err, stdout, stderr) {
   /*  console.log(stdout)
    console.log(stderr) */
    var output = stdout.match('(?<=StartObject)(.*)(?=EndObject)');
console.log('Output sent');
    /* response.send(stdout); */
    if(output && output[0]) {
      response.setHeader('Content-Type', 'application/json');
      response.send(JSON.parse(output[0]))
    } else {
      response.send('');
    }
  })
})

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})
